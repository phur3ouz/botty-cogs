from collections import defaultdict

import os
import discord
from discord.ext import commands

from cogs.utils import checks
from cogs.utils.chat_formatting import pagify
from cogs.utils.dataIO import dataIO

PATH = os.path.join("data", "grab")
JSON = os.path.join(PATH, "grabs.json")

def nested_dict():
    """Recursively nested defaultdict."""
    return defaultdict(nested_dict)


class Grab:
    """idanoo's grab cog - inspired by https://github.com/smlbiobot/SML-Cogs/blob/master/quotes/quotes.py"""

    def __init__(self, bot):
        self.bot = bot
        self.grabs = nested_dict()
        self.grabs.update(dataIO.load_json(JSON))

    @commands.command(pass_context=True, no_pm=True)
    async def grab(self, ctx, user: discord.Member = None):
        """Grabs the users last message!"""

        msg = ' '
        if user != None:
            # Grab users last message.
            await self.bot.say("Will grab: " + user.mention)
        else:
            # No user listed??
            await self.bot.say(ctx.message.author.mention + " Usage: .grab <username>.")


    @commands.command()
    async def grabr(self):
        """Randomgrab"""

        #Your code will go here
        await self.bot.say("Not implemented.. :)")


def check_folder():
    """Check folder."""
    if not os.path.exists(PATH):
        os.makedirs(PATH)


def check_file():
    """Check files."""
    if not dataIO.is_valid_json(JSON):
        dataIO.save_json(JSON, {})


def setup(bot):
    """Setup."""
    check_folder()
    check_file()
    bot.add_cog(Grab(bot))
