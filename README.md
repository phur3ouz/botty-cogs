# botty-cogs
General purpose cogs repository for [Red-DiscordBot](https://github.com/Twentysix26/Red-DiscordBot)

Meant to be used with Red's downloader by adding it with:  
`[p]cog repo add botty-cogs https://gitlab.com/idanoo/botty-cogs`
