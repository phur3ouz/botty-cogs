import discord
from discord.ext import commands
from .utils.dataIO import fileIO
from .utils import checks
from random import randint
from random import choice as randchoice
import os
import re


class Amazing:
    """idanoos amazing bot"""
    
    def __init__(self,bot):
        self.bot = bot
        self.amazingReplace = "A-Mazing*"
        self.amazingRegex = re.compile("\\bamazing\\b")

    @checks.mod_or_permissions(manage_roles=True)
    @commands.command(pass_context=True, no_pm=True)
    async def amazing(self, ctx):
        await self.bot.say("You are A-Mazing <3")


    async def amazing_checker(self, message):
        if message.author.id == self.bot.user.id:
            return

        server = message.server
        lower = message.content.lower()
        if ' ' not in lower:
            return

        lowerm = re.sub(self.amazingRegex,"",lower,1)
        if lowerm == lower:
            return

        await self.bot.send_message(message.channel, "A-Mazing*")


def setup(bot):
    n = Amazing(bot)
    bot.add_listener(n.amazing_checker, "on_message")
    bot.add_cog(n)